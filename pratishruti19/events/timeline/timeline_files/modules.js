$(document).ready(function () {
    // ####################################################
    // ##
    // ## REALTIME DATABASE (Firestore Cloud Database)
    // ##
    // ## Docs: https://firebase.google.com/docs/firestore/quickstart
    // ##
    // ####################################################
    // Configs
    var highScoreContainer = $('#game-high-score .score-list');
    var firestore = firebase.firestore();
    var settings = { timestampsInSnapshots: true };
    firestore.settings(settings);
    var highScores = [];
    var fullHighScores = [];
    var highScoresJson = [];
    var db = firebase.firestore();
    window.db = db;
    var index = 1;
    db.collection('highScores').orderBy('score', 'desc').limit(10).get().then(function (querySnapshot) {
        highScoresJson = querySnapshot.docs;
        querySnapshot.forEach(function (doc) {
            var item = '<h2>' + index + '. <span>' + doc.data().name + ' -</span> ' + doc.data().score + '</h2>';
            fullHighScores.push(item);
            highScores.push(doc.data().score);
            index++;
        });
    }).then(function () {
        window.highScores = highScores;
        window.fullHighScores = fullHighScores;
        window.highScoresJson = highScoresJson;
        highScoreContainer.html(fullHighScores);
    });

    var isMobile = false;
    
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        isMobile = true;
    
        $("#instuctions-text").html("HOLD FINGER TO SCREEN TO GO, TILT PHONE TO TURN RIGHT AND LEFT");
        $(".tutorial-arrows").attr('src', "images/mobile-tutorial.png");
        $("#audio_src").attr('src', "music/ace_mobile.png");
    }

    // Submit high score
    $('#high-score-form').on('submit', function (e) {
        e.preventDefault();
        var first = $('#initial-first').val();
        var second = $('#initial-second').val();
        var third = $('#initial-third').val();
        var name = first + second + third;
        var score = parseInt($('#final-score').text(), 10);

        // Signin user anonymously to write data
        firebase.auth().signInAnonymously().then(function () {
            // Write Data to database
            db.collection('highScores').add({
                name: name.toUpperCase(),
                score: score
            }).then(function (docRef) {
                console.log('Score added: ', docRef.id);

                $( "#game-high-score").fadeIn( "slow", function() {
                  $('#modal-close').css('display', 'block');
                  $('.game-controls').css('display', 'flex');

                  $('#game-high-score .score-list').html(window.fullHighScores);
                  var index = 1;

                  db.collection('highScores').orderBy('score', 'desc').limit(10).get().then(function (querySnapshot) {
                      highScoreContainer.html('');
                      querySnapshot.forEach(function (doc) {
                          var item = '<h2>' + index + '. <span>' + doc.data().name + ' -</span> ' + doc.data().score + '</h2>';
                          highScoreContainer.append(item);
                          index++;
                      });
                  });

                });

                //    sendSlackMessage(score, name.toUpperCase())
            }).catch(function (error) {
                console.log('Error adding score: ', error);
            });
        }).catch(function (error) {
            console.log('Error signing in...', error);
        });
    });
    // ####################################################

});
