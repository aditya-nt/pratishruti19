var App, i, img, imgSrcs, imgs, j, k, len, len1,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

App = (function() {
  function App() {
    this.resize = bind(this.resize, this);
    this.photoToggle = bind(this.photoToggle, this);
    this.photoViewer = bind(this.photoViewer, this);
    this.videoEnd = bind(this.videoEnd, this);
    this.videoPlayer = bind(this.videoPlayer, this);
    this.muteAllToggle = bind(this.muteAllToggle, this);
    this.muteVideo = bind(this.muteVideo, this);
    this.muteGame = bind(this.muteGame, this);
    this.navClick = bind(this.navClick, this);
    this.navToggle = bind(this.navToggle, this);
    this.copyUrl = bind(this.copyUrl, this);
    this.closeShare = bind(this.closeShare, this);
    this.openShare = bind(this.openShare, this);
    this.scrollCheck = bind(this.scrollCheck, this);
    var i, j, len, nav_links, req;
    this.h = window.innerHeight;
    this.w = window.innerWidth;
    if (this.h < 1000) {
      TweenLite.to("#clouds", 1, {
        y: -100
      });
      TweenLite.to("#front-trees, #signs, #road, #landscape, #car-holder", 1, {
        y: 100
      });
    }
    if (this.w < 1062) {
      document.getElementById("height-adjustment").style.height = 78000;
    }
    window.onresize = (function(_this) {
      return function(e) {
        _this.h = window.innerHeight;
        _this.w = window.innerWidth;
        if (_this.h < 1000) {
          TweenLite.to("#clouds", 1, {
            y: -100
          });
          TweenLite.to("#front-trees, #signs, #road, #landscape, #car-holder", 1, {
            y: 100
          });
        } else {
          TweenLite.to("#clouds", 1, {
            y: 0
          });
          TweenLite.to("#front-trees, #signs, #road, #landscape, #car-holder", 1, {
            y: 0
          });
        }
        if (_this.w < 1062) {
          return document.getElementById("height-adjustment").style.height = 78000;
        } else {
          return document.getElementById("height-adjustment").style.height = 67000;
        }
      };
    })(this);
    this.scrollPos = 0;
    this.lastPos = 0;
    this.speed = 0;
    this.speeding = false;
    this.speedDir = null;
    this.hzScroll = true;
    this.ignoreNext = false;
    this.lastScroll = 0;
    this.lastX = 0;
    this.lastY = 0;
    this.vidTv = document.getElementById('vid');
    this.videoUI = document.getElementById('video-play');
    this.videoPlayBtn = document.getElementById('vid-click-grabber');
    this.videoMiniUI = document.getElementById('video-mini-ui');
    this.vidTv.addEventListener('ended', this.videoEnd, false);
    this.vidPlaying = false;
    this.vidStarted = false;
    this.muttedByAll = false;
    this.game_music = document.getElementById('music');
    this.gameMute = document.getElementById('game-mute');
    this.muteBtn = document.getElementById('mute-btn');
    this.muteImg = document.getElementById('mute-img');
    this.vidMute = document.getElementById('vid-mute');
    this.muteBtn.addEventListener("click", this.muteAllToggle);
    this.vidMute.addEventListener("click", this.muteVideo);
    this.gameMute.addEventListener("click", this.muteGame);
    this.shareBtn = document.getElementById('share-btn');
    this.sharePopup = document.getElementById('share-popup');
    this.sharePopupClose = document.getElementById('popup-close');
    this.copyUrlBtn = document.getElementById('copy-url');
    this.shareBtn.addEventListener("click", this.openShare);
    this.sharePopupClose.addEventListener("click", this.closeShare);
    this.copyUrlBtn.addEventListener("click", this.copyUrl);
    window.addEventListener("scroll", this.scrollCheck);
    window.onbeforeunload = (function(_this) {
      return function() {
        return window.scrollTo(0, 0);
      };
    })(this);
    TweenLite.to("#end-stats-wrapper", 0, {
      x: 0,
      rotation: 0.001
    });
    this.navBtn = document.getElementById("nav-toggle");
    this.nav = document.getElementById("nav");
    this.navBtn.addEventListener('click', this.navToggle, false);
    nav_links = document.getElementsByClassName("nav-link");
    for (j = 0, len = nav_links.length; j < len; j++) {
      i = nav_links[j];
      i.addEventListener('click', this.navClick, false);
    }
    this.videoPlayer();
    document.getElementById("scroll-block").addEventListener('click', (function(_this) {
      return function() {
        return window.scroll({
          top: 5500,
          behavior: 'smooth'
        });
      };
    })(this));
    document.getElementById("start-over-btn").addEventListener('click', (function(_this) {
      return function() {
        return window.scroll({
          top: 0,
          behavior: 'smooth'
        });
      };
    })(this));
    this.photos = document.getElementsByClassName("photo");
    this.photoViewer();
    req = new XMLHttpRequest();
    req.open('GET', 'videos/Gambler_500_RH_480.mp4', true);
    req.responseType = 'blob';
    req.onload = (function(_this) {
      return function() {
        return console.log("video loaded");
      };
    })(this);
    req.onerror = (function(_this) {
      return function() {
        return "video error";
      };
    })(this);
  }

  App.prototype.scrollCheck = function(e) {
    var statsPos, vidPos;
    this.scrollPos = window.scrollY;
    if (this.scrollPos > 6000) {
      statsPos = document.getElementById("end-stats-wrapper")._gsTransform.x;
      if (statsPos <= -13230) {
        TweenLite.to("#end-stats-wrapper", 0, {
          x: -13230
        });
        this.hzScroll = false;
        TweenLite.to("#end-stats", 0.3, {
          y: ((this.scrollPos * 0.3) - 13230) * -0.5
        });
        if ((this.scrollPos * 0.3) < 13230) {
          this.hzScroll = true;
        }
      }
    } else {
      this.hzScroll = true;
    }
    if (this.hzScroll) {
      this.speed = this.scrollPos - this.lastPos;
      this.lastPos = this.scrollPos;
      this.dirtSpeed = 200;
      if ((this.speed > this.dirtSpeed || this.speed < this.dirtSpeed * -1) && this.speeding === false) {
        this.speeding = true;
        if (this.speed > this.dirtSpeed) {
          TweenLite.to(".car-dust-fw", 0.15, {
            opacity: 1
          });
          this.speedDir = "forward";
          TweenLite.to("#car-body", 0.25, {
            rotation: -0.75,
            transformOrigin: "left 75%"
          });
        } else {
          TweenLite.to(".car-dust-bk", 0.15, {
            opacity: 1
          });
          this.speedDir = "backward";
          TweenLite.to("#car-body", 0.25, {
            rotation: 0.75,
            transformOrigin: "right 75%"
          });
        }
        TweenLite.to(".car-wheel", 0.25, {
          webkitFilter: "blur(1px)"
        });
        setTimeout(this.scrollCheck, 100);
      }
      if (this.speeding === true && this.speed < this.dirtSpeed && this.speed > this.dirtSpeed * -1) {
        this.speeding = false;
        TweenLite.to(".car-dust", 0.15, {
          opacity: 0
        });
        if (this.speedDir === "forward") {
          TweenLite.to("#car-body", 0.25, {
            rotation: 0,
            transformOrigin: "left 75%"
          });
        }
        if (this.speedDir === "backward") {
          TweenLite.to("#car-body", 0.25, {
            rotation: 0,
            transformOrigin: "right 75%"
          });
        }
        TweenLite.to(".car-wheel", 0.25, {
          webkitFilter: "blur(0px)"
        });
      }
      TweenLite.to("#sky", 0.3, {
        left: this.scrollPos * -0.1,
        rotation: 0.001
      });
      TweenLite.to("#landscape", 0.3, {
        x: this.scrollPos * -0.25,
        rotation: 0.001
      });
      TweenLite.to(".car-wheel", 0.315, {
        rotation: "+=" + (this.speed * 1),
        transformOrigin: "50 50%",
        ease: Power0.easeNone
      });
      TweenLite.to("#road, #top-content, #end-stats-wrapper", 0.3, {
        x: this.scrollPos * -0.3,
        rotation: 0.001
      });
      TweenLite.to("#signs", 0.3, {
        x: this.scrollPos * -0.5,
        rotation: 0.001
      });
      TweenLite.to("#front-trees, #clouds", 0.3, {
        x: this.scrollPos * -0.4,
        rotation: 0.001
      });
      if (this.vidPlaying) {
        vidPos = document.getElementById("road")._gsTransform.x;
        if (vidPos < -2700) {
          this.vidTv.pause();
          this.videoUI.classList.remove("playing");
          return this.vidPlaying = false;
        }
      }
    }
  };

  App.prototype.openShare = function(e) {
    return this.sharePopup.style.display = "flex";
  };

  App.prototype.closeShare = function(e) {
    return this.sharePopup.style.display = "none";
  };

  App.prototype.copyUrl = function(e) {
    var el, range;
    el = document.createElement('textarea');
    el.value = "http://gambler500.roundhouseagency.com";
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    range = document.createRange();
    range.selectNode(document.getElementById("url-box"));
    window.getSelection().removeAllRanges();
    return window.getSelection().addRange(range);
  };

  App.prototype.navToggle = function(e) {
    e.stopPropagation();
    e.preventDefault();
    if (this.nav.classList.contains("open")) {
      this.nav.classList.remove("open");
      e.target.classList.remove("open");
      return TweenLite.to("#nav", 0.3, {
        height: "40px",
        ease: Power2.easeInOut
      });
    } else {
      this.nav.classList.add("open");
      e.target.classList.add("open");
      return TweenLite.to("#nav", 0.3, {
        height: "235px",
        ease: Power2.easeInOut
      });
    }
  };

  App.prototype.navClick = function(e) {
    var scrollOfset;
    if (e.target.id === "nav-start") {
      return window.scroll({
        top: 0,
        behavior: 'smooth'
      });
    } else if (e.target.id === "nav-vid") {
      scrollOfset = window.innerWidth - (580 / 2);
      return window.scroll({
        top: 6450 - scrollOfset,
        behavior: 'smooth'
      });
    } else if (e.target.id === "nav-photos") {
      return window.scroll({
        top: 11500,
        behavior: 'smooth'
      });
    } else if (e.target.id === "nav-game") {
      scrollOfset = window.innerWidth - (625 / 2);
      return window.scroll({
        top: 20550 - scrollOfset,
        behavior: 'smooth'
      });
    } else if (e.target.id === "nav-photos-2") {
      return window.scroll({
        top: 24000,
        behavior: 'smooth'
      });
    } else if (e.target.id === "nav-end") {
      return window.scroll({
        top: 44100,
        behavior: 'smooth'
      });
    } else {
      return console.log("????");
    }
  };

  App.prototype.muteGame = function() {
    if (this.gameMute.classList.contains("muted")) {
      this.game_music.muted = false;
      this.muteImg.classList.add("audio");
      return this.gameMute.classList.remove("muted");
    } else {
      this.game_music.muted = true;
      this.muteImg.classList.remove("audio");
      return this.gameMute.classList.add("muted");
    }
  };

  App.prototype.muteVideo = function() {
    if (this.vidMute.classList.contains("muted")) {
      this.vidTv.muted = false;
      this.muteImg.classList.add("audio");
      return this.vidMute.classList.remove("muted");
    } else {
      this.vidTv.muted = true;
      this.muteImg.classList.remove("audio");
      return this.vidMute.classList.add("muted");
    }
  };

  App.prototype.muteAllToggle = function() {
    if (this.muteImg.classList.contains("audio")) {
      this.vidTv.muted = true;
      this.game_music.muted = true;
      this.muteImg.classList.remove("audio");
      this.vidMute.classList.add("muted");
      return this.gameMute.classList.add("muted");
    } else {
      this.game_music.muted = false;
      this.vidTv.muted = false;
      this.muteImg.classList.add("audio");
      this.vidMute.classList.remove("muted");
      return this.gameMute.classList.remove("muted");
    }
  };

  App.prototype.videoPlayer = function() {
    return this.videoPlayBtn.addEventListener('click', (function(_this) {
      return function() {
        if (_this.vidPlaying) {
          _this.vidTv.pause();
          _this.videoUI.classList.remove("playing");
          _this.videoUI.classList.add("paused");
          return _this.vidPlaying = false;
        } else if (_this.vidStarted) {
          _this.vidTv.play();
          _this.vidPlaying = true;
          _this.videoUI.classList.remove("paused");
          return _this.videoUI.classList.add("playing");
        } else {
          _this.vidTv.currentTime = 0;
          _this.vidTv.play();
          _this.vidPlaying = true;
          _this.videoUI.classList.add("playing");
          _this.vidStarted = true;
          _this.vidTv.muted = false;
          _this.muteBtn.classList.add("audio");
          return _this.muteImg.classList.add("audio");
        }
      };
    })(this));
  };

  App.prototype.videoEnd = function(e) {
    this.vidTv.currentTime = 0.5;
    this.videoUI.classList.remove("playing");
    return this.videoUI.classList.remove("paused");
  };

  App.prototype.photoViewer = function() {
    var i, j, len, ref, results;
    ref = this.photos;
    results = [];
    for (j = 0, len = ref.length; j < len; j++) {
      i = ref[j];
      results.push(i.addEventListener('click', this.photoToggle, false));
    }
    return results;
  };

  App.prototype.photoToggle = function(e) {
    var closeBtn, i, img, j, len, precentOfScreen, ref, target;
    precentOfScreen = 90;
    if (e.target.classList.contains("rez-photo")) {
      target = e.target.parentElement;
    } else {
      target = e.target;
    }
    img = target.querySelector(".rez-photo");
    closeBtn = target.querySelector(".close-icon");
    if (target.classList.contains("open")) {
      target.classList.remove("open");
      target.style.width = "";
      target.style.height = "";
      target.style.top = "";
      return target.style.marginLeft = 0;
    } else {
      ref = this.photos;
      for (j = 0, len = ref.length; j < len; j++) {
        i = ref[j];
        if (i.classList.contains("open")) {
          i.classList.remove("open");
          i.style.width = "";
          i.style.height = "";
          i.style.top = "";
          i.style.marginLeft = 0;
        }
      }
      target.classList.add("open");
      return this.resize(img, precentOfScreen, closeBtn, target);
    }
  };

  App.prototype.resize = function(image, precentOfScreen, closeBtn, holder) {
    var imageAspectRatio, imageHeight, imageHeightResizedTarget, imageOrientation, imageResizedByHeight, imageResizedByWidth, imageWidth, imageWidthResizedTarget, windowX, windowY;
    windowX = document.documentElement.clientWidth;
    windowY = document.documentElement.clientHeight;
    imageWidthResizedTarget = windowX * (precentOfScreen / 100);
    imageHeightResizedTarget = windowY * (precentOfScreen / 100);
    imageWidth = image.naturalWidth;
    imageHeight = image.naturalHeight;
    imageAspectRatio = image.naturalWidth / image.naturalHeight;
    if (imageAspectRatio > 1) {
      imageOrientation = "Landscape";
    } else if (imageAspectRatio < 1) {
      imageOrientation = "Portrait";
    } else {
      imageOrientation = "Square";
    }
    imageResizedByWidth = [imageWidthResizedTarget, (imageWidthResizedTarget * imageHeight) / imageWidth];
    imageResizedByHeight = [(imageHeightResizedTarget * imageWidth) / imageHeight, imageHeightResizedTarget];
    if (imageResizedByWidth[0] <= imageWidthResizedTarget && imageResizedByWidth[1] <= imageHeightResizedTarget) {
      return TweenLite.to(holder, 0.3, {
        width: imageResizedByWidth[0],
        height: imageResizedByWidth[1],
        marginLeft: imageResizedByWidth[0] / -4
      });
    } else if (imageResizedByHeight[0] <= imageWidthResizedTarget && imageResizedByHeight[1] <= imageHeightResizedTarget) {
      return TweenLite.to(holder, 0.3, {
        top: "40px",
        width: imageResizedByHeight[0],
        height: imageResizedByHeight[1],
        marginLeft: imageResizedByHeight[0] / -4
      });
    } else {
      return console.log("Error!");
    }
  };

  return App;

})();

this.loadcount = 0;

this.percent = 0;

this.startApp = false;

this.siteBody = document.getElementById("fixed-wrapper");

this.pText = document.getElementById("perecent-value");

imgs = document.getElementsByTagName("img");

imgSrcs = [];

for (j = 0, len = imgs.length; j < len; j++) {
  i = imgs[j];
  imgSrcs.push(i.src);
}

this.loadtotal = imgSrcs.length;

for (k = 0, len1 = imgSrcs.length; k < len1; k++) {
  i = imgSrcs[k];
  img = new Image();
  img.src = i;
  img.onload = (function(_this) {
    return function() {
      var _app;
      _this.loadcount++;
      _this.percent = Math.round((_this.loadcount / _this.loadtotal) * 100);
      _this.pText.innerHTML = _this.percent;
      if (_this.percent === 100 && _this.startApp === false) {
        _this.startApp = true;
        _this.siteBody.style.display = "block";
        $("#preloader").fadeOut("slow");
        window.scrollTo(0, 0);
        _app = new App();
      }
    };
  })(this);
}
