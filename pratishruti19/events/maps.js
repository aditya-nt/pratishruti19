
var icons = { parking: { icon: 'clock5.png' },parking1: { icon: 'clock5.png' }};


// REPLACE WITH DATA FROM API
//TITLE | POSITION - LAT , LNG | ICON | TITLE | CONTENT
var airports = [
	{ 
		title: 'INGENIUM', 
		position: { 
			lat: 21.176973, 
			lng: 79.062048 }, 
		icon: 'parking1',	
		content: '<div id="content"><iframe src="https://360player.io/p/awNHSk/" frameborder="0" width=560 height=315 allowfullscreen data-token="awNHSk"></iframe></div>'
	},
	{ 
		title: 'FY BLOCK', 
		position: { 
			lat: 21.177358, 
			lng: 79.060912}, 
		icon: 'parking',	
		content: '<div id="content"><iframe src="https://360player.io/p/BBY7Bj/" frameborder="0" width=560 height=315 allowfullscreen data-token="BBY7Bj"></iframe></div>'
	},
	{ 
		title: 'RATTLEHEAD', 
		position: { 
			lat: 21.177594, 
			lng: 79.060745 }, 
		icon: 'parking',	
		content: '<div id="content"><iframe src="https://360player.io/p/qWY3vv/" frameborder="0" width=560 height=315 allowfullscreen data-token="qWY3vv"></iframe></div>'
	},
	{ 
		title: 'BEATMAKERS ARENA', 
		position: { 
			lat: 21.177969, 
			lng: 79.061058 }, 
		icon: 'parking',	
		content: '<div id="content"><iframe src="https://360player.io/p/6qJyQV/" frameborder="0" width=560 height=315 allowfullscreen data-token="6qJyQV"></iframe></div>'
	},
	{ 
		title: 'DUNK IT', 
		position: { 
			lat: 21.177405, 
			lng: 79.062044 }, 
		icon: 'parking',	
		content: '<div id="content"><iframe src="https://360player.io/p/FNTGv9/" frameborder="0" width=560 height=315 allowfullscreen data-token="FNTGv9"></iframe></div>'
	}

];

function initMap() {
	
	var RCOEM = { 
			lat: 21.176602,
			lng: 79.061117  };
	var mapOptions = {
  zoom: 17,
 center: RCOEM,
  mapTypeId: 'satellite'
};
	var map = new google.maps.Map( document.getElementById('map'),mapOptions);
		  
	var InfoWindows = new google.maps.InfoWindow({});
	
	airports.forEach(function(airport) {	
		var marker = new google.maps.Marker({
		  position: { lat: airport.position.lat, lng: airport.position.lng },
		  map: map,
		  icon: icons[airport.icon].icon,
		  title: airport.title
		});
		marker.addListener('click', function() {
		  InfoWindows.open(map, this);
		  InfoWindows.setContent(airport.content);
		});
	});

	       var flightPlanCoordinates = [
          {lat: 21.177939, lng: 79.059175}, 
          {lat: 21.178166, lng: 79.060954}, 
          {lat: 21.178489, lng: 79.062724}, 
          {lat: 21.178160, lng: 79.062744}, 
          {lat: 21.178135, lng: 79.062967},
          {lat: 21.177587, lng: 79.063152},
          {lat: 21.177624, lng: 79.062585},
          {lat: 21.176095, lng: 79.061748},
          {lat: 21.176495, lng: 79.060592},
          {lat: 21.176861, lng: 79.060488},
          {lat: 21.177939, lng: 79.059175},
        ];
        var flightPath = new google.maps.Polyline({
          path: flightPlanCoordinates,
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });

        flightPath.setMap(map);
}