var imgArr = [
	"../../flyers/SOCCER%20STORM.png",//A1
	"../../flyers/INGENIUM.png",//A2
	"../../flyers/DUNK%20IT.png",//A3
	"../../flyers/MIN%202%20WIN%20IT.png",//A4
	"../../flyers/NATURES%20VOICE.png",//A5
	"../../flyers/RAPID%20CHESS.png",//A6
	"../../flyers/LAN%20GAMING.png",//B1
	"../../flyers/VIBRATO.png",//B2
	"../../flyers/KWIK%20CRICKET.png",//B3
	"../../flyers/BON%20VOYAGE.png",//B4
	"../../flyers/DALAL%20STREET.png",//B5
	"../../flyers/PERSONA.png",//B6
	"../../flyers/PICASSO.png",//C1
	"../../flyers/PUBG.png",//C2
	"../../flyers/QUIZZEUS.png",//C3
	"../../flyers/ART%20AFFAIR.png",//C4
	"../../flyers/TAKE%20DOWN.png",//C5
	"../../flyers/CHITRA%20SANDESH.png",//C6
	"../../flyers/ETHICAL%20HACKING.png",//D1
	"../../flyers/ARTIFICIAL%20INTELLIGENCE.png",//D2
	"../../flyers/BEATMAKERS%20ARENA.png",//D3
	"../../flyers/BEACH%20VOLLEY.png",//D4
	"../../flyers/FOOTSTEPS.png",//D5
	"../../flyers/PITCH%20YOUR%20IDEA.png",//D6
	"../../flyers/HUMAN%20FOOSBALL.png",//E1
	"../../flyers/NAQAAB.png",//E2
	"../../flyers/RATTLEHEAD.png",//E3
	"../../flyers/LIVE%20CS.png",//E4
	"../../flyers/STIMULUS.png",//E5
	"../../flyers/DANCE%20WORKSHOP.png",//E6
];
var evName = [
	"soccer Storm",
	"ingenium",
	"Dunk it",
	"Minute 2 win it",
	"nature's voices",
	"rapid chess",
	"lan gaming",
	"vibrato",
	"kwik cricket",
	"Bon voyage",
	"dalal street",
	"persona",
	"picasso",
	"pubg",
	"quizzeus",
	"art affair",
	"take down",
	"chitra sandesh",
	"ethical hacking",
	"Artificial intelligence",
	"Beatmakers Arena",
	"beach volley",
	"footsteps",
	"pitch your idea",
	"human foosball",
	"naqaab",
	"rattlehead",
	"live counter strike",
	"stimulus",
	"dance workshop",
];
var content = [
	"Football played in a non conventional way which brings in two teams of four against one another in a limited area with goalposts of reduced size.The rules are changed to fit the changes as well as to make it more interesting like no player is allowed to stand inside the D marked area for more than 3 seconds and no lofted shots are allowed.",
	"A three day, team based event with each team comprising of five members with at least two girls. The teams participate in various tasks that seemingly challenge their physical as well as mental strength. It generally starts with a treasure hunt followed by a round go tug-of-war. The following rounds are redesigned each year to provide an adrenaline rush to the students.",
	"A three versus three basketball match played in half court with modified rules. These modified rules of the match increase the thrills of the sport and the desire to win. It includes the basic rules such as travelling and outside the court and the fouls are same as any basketball match.",
	"A minute might not seem important, but it is all you have! It is a physical event that tests the mental alertness and activeness of the participants. All the participants are given 1 minute physical tasks and they are required to complete them as soon as possible. They can participate in a team of two or solo for this 2 day event.",
	"What more beautiful than the nature itself to calm the restless mind. It is a new event in which we aim to help students overcome their stress, depression and anxiety problems by making them listen to calming nature's voices. This might help student to live a healthy, stress-free life. Students can then get back to their normal life with a optimistic approach.",
	"A rapid chess game of 30 minutes will be played. It will be a knockout tournament. Rules: 1) Maximum 2 illegal moves, player will lose the game on 3rd illegal. 2) Both players will have a clock, clock rules applied.",
	"For all those gamers out there looking for an opportunity to show-off their unusual talent, hold your horses because here comes Pratishruti LAN gaming fest where in this event you will compete in some of the best and most played games on the table. Conquer the gaming arena in most popular global games FIFA and Counter Strike   and  be the ultimate Gaming King.",
	"Vibrato brings to you a myriad of musical events at one place. Whether you choose to mesmerize the crowd with your soulful voice or control the vibes with gentle percussion. It is conducted over two days and has three categories namely solo, duet and instrumental, each having a winner. ",
	"Be your own Dhoni. It is a simplified and fast version of cricket played in a netted area.  It is played with sponge ball, bats and stumps, and therefore doesn't require protective equipment like pads, gloves, etc.  The pitch is much smaller than a hardball pitch and therefore less efforts are required for the game.",
	"Bored of the conventional Snakes & Ladder or Ludo, and looking for some adrenaline rush? Voyage is the ultimate battle of board games, where you can delve into playing board games - that are no child's play, but will still remind you of your childhood !!",
	"Do you want to learn how to make money in the stock market, but do not know where to start? Do you want to trade stocks on your fingertips and BUY and SELL stocks on your own? Do you want to analyze a company based on its financial numbers and figures? If your answers are YES! then this 6 hours session can be a 'Life Changing Event' for you. It allows individuals to buy and sell stocks in accordance with the live stock market scenario",
	"Appearances make impressions but it is the personality that makes an impact.  Persona is a flagship event of Pratishruti that provides a platform to the students to showcase their talents. The students participate individually and are judged on their personalities. It is a four day  event which includes an aptitude round, group discussions, personal interviews, talent hunts and a final public interview round.",
	"Picasso is a photography competition for amateur as well as professional photographers. The competition takes place in three rounds each having a different theme. The participants mail their photos based on the theme within the given time and are judged on the basis of their creativity and interpretation of the theme.",
	"Pick up your best squad and go on a ramapage in battle ground or be a one man army. Be a champion and win cash prize worth RS 2000 at miniscule fee of 200 for squad and 80 for solo. Be a part of event and be the ultiamte champion. Its not just a game its a battle royale! #WinnerWinnerChickenDinner!!!",
	"A Quiz competition which judges your general knowledge, awareness, mental reasoning and vocabulary. With quizzes ranging across a range of topics like sports to our diverse culture, get ready to rack those brains. It consist of two rounds, first comprises of an Aptitude Test and the second round is disclosed on spot. Students can participate individually or in pairs.",
	"For all holding a brush or a pencil out there, Art Affairs provides you a stunning range of events from beautifying a dull wall to brushless painting, from face painting to nail art, from doodling to rendering the world and the complexity within in bright shades, the challenge is on.",
	"You dont need to be a sumo to win a wrestling. Take down is basically an arm wrestling event in which we have two participants competing against one another. Both the participants sit facing each other and try to bent each other's elbow to make other's hand touch the surface. The one who does it first, wins the game.",
	"Organised for the first time, a sanskrit based event where participants will have to make a picture of any of the given topics in sanskrit. Topics for this competetion are: science and technology, people's perceptions about sanskrit, pictures based on kalidas's  works and many more!",
	"The Ethical Hacking Workshop will ready students for the Information Security Industry with technical training on mobile penetration testing. Live hacking sessions will allow in-depth understanding of computer server and network systems, which is essential for formulating strategies for improved security. It will be conducted by Mahesh Rakheja a famous Technical Public speaker and Startup entrepreneur.",
	"This workshop provides a common platform for showcasing recent innovation of AI and deep learning in software development and IT Industry. Deep Learning and Machine Learning in various verticals of real life including Health Care, Digital Learning, Business, Transport, Finance, Social Networking, etc. This workshop, presented by MyCaptain and conducted by Mr. Yuvraj Jeswal aims at providing students a hands-on AI experience.",
	"What's the first thing that comes to the mind when you hear the word Arena? Warriors and Weapons? But this time it's different. There's definitely an arena and warriors with weapons. But these weapons won't hurt or kill, instead make you groove, dance and  kill it in a different way. This event deals with EDM artist around the region performing their best tune and people grooving on their beats!",
	"Beach Volleyball is a popular team sport where two teams of two players are separated by a net. Each team tries to score points by grounding a ball on the other team's court. The first two touches are used to set up for an attack, an attempt to direct the ball back over the net in such a way that the serving team is unable to prevent it from being grounded in their court.",
	"Footsteps is a shoutout to all the dancers out there to express themselves with the best moves while they are at it. It is a two level event carried out in three categories: 1) Solo 2) Duet 3) Group . Two divisions are made in the dance form ie. 1) Classical  2) Western",
	"Nothing can stop a bulb from glowing.As the name suggests, this event is about bringing up your own business ideas and getting the necessary help from RCOEM's TBI. It is a two rounds event, first being the presentation round and second being Model Round. The participants whose idea will be selected will get funds to actually start up their business and guidance from TBI.",
	"Wanted to live the F.R.I.E.N.D.S. life? Here's the deal. Human Foosball is our life-sized version of table soccer! It is a game played in an inflatable arena wherein two teams of five try to score against each other while attached to poles. The freedom as well as the bondage of the game is what makes it attractive and interesting.",
	"Dramatics lets you step out of the life as you're living it now, and step into someone else's shoes. It helps you escape the monotonous routine life to enter into one with details and design of your own. The stage is set for you with competitions like stage plays and monologues. Skits allow a team of at most eight actors and production artists to stage their acts in English, hindi or Marathi.",
	"Rattlehead is a unique melting pot of music that allows the local music bands to feature their work along with some grooving performances in rock music. The completion is conducted over two evenings. The first round is an elimination round and the finals take place on the opening evening “AARAMBH” of Pratishruti.",
	"The warrior is always inside us, so this is the battle field. What happens when a game on virtual platform is presented to u in the real world... This event deals with participants entering the arena with body armour and weapons and surviving the given time or uptil the last man stands. At a time 4-5 players enter the arena.",
	"Stimulus is a literature based event under the flagship of Pratushruti that is conducted over two days in three rounds. Students can participate individually or in pairs. The rounds include basic grammar, spoken English as well as vocabulary. Each category has a separate winner.",
	"The first letters we learn is ABCD and that's what this event's aim is i.e. 'Any Body Can Dance'.  It is a two-day event in which we bring out the best of participants by making them learn dance from professionals. This boosts their confidence, improves their personality and helps them gain stage confidence.",
];

var contact1 = [
	"Aditya Tripathi",//A1
	"Uddesh Chhajed",//A2
	"Akash Chopade",//A3
	"Raghav Mundhada",//A4
	"Omkar Sarda",//A5
	"Purvesh Agrawal",//A6
	"Aditya Tripathi",//B1
	"Purvesh Agrawal",//B2
	"Yash Khandelwal",//B3
	"Yash Khandelwal",//B4
	"Omkar Sarda",//B5
	"Yash Khandelwal",//B6
	"Jasmine Jadwani",//C1
	"Aditya Tripathi",//C2
	"Purvesh Agrawal",//C3
	"Adarshita Khan",//C4
	"Yash Khandelwal",//C5
	"Purvesh Agrawal",//C6
	"Gagan Chordiya",//D1
	"Gagan Chordiya",//D2
	"Arjun Agrawal",//D3
	"Ayush Tripathi",//D4
	"Shambhavi Kumar",//D5
	"Siddharth Gupta",//D6
	"Ayush Tripathi",//E1
	"Siddharth Gupta",//E2
	"Akash Chopade",//E3
	"Arjun Agrawal",//E4
	"Justin Joseph",//E5
	"Shambhavi Kumar",//E6
];
var contact2 = [
	"Raghav Mundhada",//A1
	"Justin Joseph",//A2
	"Sanil Andhare",//A3
	"Aishwarya Mahatme",//A4
	"Sachi Fulwani",//A5
	"Yash Khandelwal",//A6
	"Yash Welekar",//B1
	"Pragya Singh",//B2
	"Mohit Rathi",//B3
	"Shambhavi Kumar",//B4
	"Bhagyesh Chandak",//B5
	"Punit Bhatia",//B6
	"Rohee Gupta",//C1
	"Yash Welekar",//C2
	"Adarshita Khan",//C3
	"Nancy Tolani",//C4
	"Gagan Chordiya",//C5
	"Chetan Pandey",//C6
	"Punit Bhatia",//D1
	"Punit Bhatia",//D2
	"Aryan Hedau",//D3
	"Bhagyesh Chandak",//D4
	"Rohee Gupta",//D5
	"Yash Khandelwal",//D6
	"Mohit Rathi",//E1
	"Aditya Tripathi",//E2
	"Pragya Singh",//E3
	"Yash Welekar",//E4
	"Shivani Khatri",//E5
	"Jasmine Jadwani",//E6
];
var doc1link="https://drive.google.com/file/d/1S-i5U-2ffTNeJSPhppF24C3Dq22Zbt9Q/view?usp=drivesdk";
var walink1 = [
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Ingenium!!!",//A2
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Dunk%20It!!!",//A3
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Minute%20to%20win%20it!!!",//A4
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Nature's%20Voices!!!",//A5
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Rapid%20Chess!!!",//A6
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Lan%20Gaming!!!",//B1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Vibrato!!!",//B2
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Kwik%20Cricket!!!",//B3
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Bon%20Voyage!!!",//B4
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Dalaal%20Street!!!",//B5
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Persona!!!",//B6
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Picasso!!!",//C1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Pubg!!!",//C2
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Quizzeus!!!",//C3
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Art%20Affairs!!!",//C4
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Take%20Down!!!",//C5
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Chitra%20Sandesh!!!",//C6
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Ethical%20Hacking!!!",//D1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Ethical%20Hacking!!!",//D1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Ethical%20Hacking!!!",//D1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Ethical%20Hacking!!!",//D1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Ethical%20Hacking!!!",//D1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Ethical%20Hacking!!!",//D1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Ethical%20Hacking!!!",//D1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Ethical%20Hacking!!!",//D1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Ethical%20Hacking!!!",//D1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Ethical%20Hacking!!!",//D1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Ethical%20Hacking!!!",//D1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Ethical%20Hacking!!!",//D1
];
var walink2 = [
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
	"https://wa.me/918657774627?text=Hey%20there,%20I%20want%20to%20participate%20in%20Soccer%20Storm!!!",//A1
];
var locate = window.location;
		document.card.indexx.value = locate;

		var text = document.card.indexx.value;

		function delineate(str)
		{
			theleft = str.indexOf("=") + 1;
			theright = str.lastIndexOf("&");
			return(str.substring(theleft, theright));
		}

		var i = parseInt(delineate(text),10);

var locatetext = window.location;
		document.card.indexx.value = locatetext;

		var text = document.card.indexx.value;

		function delineate(str)
		{
			theleft = str.indexOf("=") + 1;
			theright = str.lastIndexOf("&");
			return(str.substring(theleft, theright));
		}

		var textvalue = delineate(text);
		
		if(textvalue == "SOCCER%20STORM"){ var i = 0; }
		if(textvalue == "INGENIUM"){ var i = 1; }
		if(textvalue == "DUNK%20IT"){ var i = 2; }
		if(textvalue == "MINUTE%202%20WIN%20IT"){ var i = 3; }
		if(textvalue == "NATURES%20VOICES"){ var i = 4; }
		if(textvalue == "RAPID%20CHESS"){ var i = 5; }
		if(textvalue == "LAN%20GAMING"){ var i = 6; }
		if(textvalue == "VIBRATO"){ var i = 7; }
		if(textvalue == "KWIK%20CRICKET"){ var i = 8; }
		if(textvalue == "BON%20VOYAGE"){ var i = 9; }
		if(textvalue == "DALAAL%20STREET"){ var i = 10; }
		if(textvalue == "PERSONA"){ var i = 11; }
		if(textvalue == "PICASSO"){ var i = 12; }
		if(textvalue == "PUBG"){ var i = 13; }
		if(textvalue == "QUIZZEUS"){ var i = 14; }
		if(textvalue == "ART%20AFFAIR"){ var i = 15; }
		if(textvalue == "TAKE%20DOWN"){ var i = 16; }
		if(textvalue == "CHITRA%20SANDESH"){ var i = 17; }
		if(textvalue == "ETHICAL%20HACKING"){ var i = 18; }
		if(textvalue == "ARTIFICIAL%20INTELLIGENCE"){ var i = 19; }
		if(textvalue == "BEATMAKERS%20ARENA"){ var i = 20; }
		if(textvalue == "BEACH%20VOLLEY"){ var i = 21; }
		if(textvalue == "FOOTSTEPS"){ var i = 22; }
		if(textvalue == "PITCH%20YOUR%20IDEA"){ var i = 23; }
		if(textvalue == "HUMAN%20FOOSBALL"){ var i = 24; }
		if(textvalue == "NAQAAB"){ var i = 25; }
		if(textvalue == "RATTLEHEAD"){ var i = 26; }
		if(textvalue == "LIVE%20CS"){ var i = 27; }
		if(textvalue == "STIMULUS"){ var i = 28; }
		if(textvalue == "DANCE%20WORKSHOP"){ var i = 29; }
		
		
		change();
		
function change(){
	$(".flyer").attr("src",imgArr[i]);
	$(".col h3").text(evName[i]);
	$(".col p").text(content[i]);
	$("#c1").text(contact1[i]);
	$("#c2").text(contact2[i]);
	$(".cont1").attr("href",walink1[i]);
	$(".cont2").attr("href",walink2[i]);
	
if(i==12){
	    $("#c0").text("Rules- Round 1 link");
}
}