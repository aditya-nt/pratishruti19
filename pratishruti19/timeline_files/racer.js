// This was a total hack job done to make this work in a week

//=========================================================================
// minimalist DOM helpers
//=========================================================================

var Dom = {

  get:  function(id)                     { return ((id instanceof HTMLElement) || (id === document)) ? id : document.getElementById(id); },
  set:  function(id, html)               { Dom.get(id).innerHTML = html;                        },
  on:   function(ele, type, fn, capture) { Dom.get(ele).addEventListener(type, fn, capture);    },
  un:   function(ele, type, fn, capture) { Dom.get(ele).removeEventListener(type, fn, capture); },
  show: function(ele, type)              { Dom.get(ele).style.display = (type || 'block');      },
  blur: function(ev)                     { ev.target.blur();                                    },

  addClassName:    function(ele, name)     { Dom.toggleClassName(ele, name, true);  },
  removeClassName: function(ele, name)     { Dom.toggleClassName(ele, name, false); },
  toggleClassName: function(ele, name, on) {
    ele = Dom.get(ele);
    var classes = ele.className.split(' ');
    var n = classes.indexOf(name);
    on = (typeof on == 'undefined') ? (n < 0) : on;
    if (on && (n < 0))
      classes.push(name);
    else if (!on && (n >= 0))
      classes.splice(n, 1);
    ele.className = classes.join(' ');
  },

  storage: window.localStorage || {}

}

//=========================================================================
// general purpose helpers (mostly math)
//=========================================================================

var Util = {

  timestamp:        function()                  { return new Date().getTime();                                    },
  toInt:            function(obj, def)          { if (obj !== null) { var x = parseInt(obj, 10); if (!isNaN(x)) return x; } return Util.toInt(def, 0); },
  toFloat:          function(obj, def)          { if (obj !== null) { var x = parseFloat(obj);   if (!isNaN(x)) return x; } return Util.toFloat(def, 0.0); },
  limit:            function(value, min, max)   { return Math.max(min, Math.min(value, max));                     },
  randomInt:        function(min, max)          { return Math.round(Util.interpolate(min, max, Math.random()));   },
  randomChoice:     function(options)           { return options[Util.randomInt(0, options.length-1)];            },
  percentRemaining: function(n, total)          { return (n%total)/total;                                         },
  accelerate:       function(v, accel, dt)      { return v + (accel * dt);                                        },
  interpolate:      function(a,b,percent)       { return a + (b-a)*percent                                        },
  easeIn:           function(a,b,percent)       { return a + (b-a)*Math.pow(percent,2);                           },
  easeOut:          function(a,b,percent)       { return a + (b-a)*(1-Math.pow(1-percent,2));                     },
  easeInOut:        function(a,b,percent)       { return a + (b-a)*((-Math.cos(percent*Math.PI)/2) + 0.5);        },
  exponentialFog:   function(distance, density) { return 1 / (Math.pow(Math.E, (distance * distance * density))); },

  increase:  function(start, increment, max) { // with looping
    var result = start + increment;
    while (result >= max)
      result -= max;
    while (result < 0)
      result += max;
    return result;
  },

  project: function(p, cameraX, cameraY, cameraZ, cameraDepth, width, height, roadWidth) {
    p.camera.x     = (p.world.x || 0) - cameraX;
    p.camera.y     = (p.world.y || 0) - cameraY;
    p.camera.z     = (p.world.z || 0) - cameraZ;
    p.screen.scale = cameraDepth/p.camera.z;
    p.screen.x     = Math.round((width/2)  + (p.screen.scale * p.camera.x  * width/2));
    p.screen.y     = Math.round((height/2) - (p.screen.scale * p.camera.y  * height/2));
    p.screen.w     = Math.round(             (p.screen.scale * roadWidth   * width/2));
  },

  overlap: function(x1, w1, x2, w2, percent) {
    var half = (percent || 1)/2;
    var min1 = x1 - (w1*half);
    var max1 = x1 + (w1*half);
    var min2 = x2 - (w2*half);
    var max2 = x2 + (w2*half);
    return ! ((max1 < min2) || (min1 > max2));
  }

}

//=========================================================================
// POLYFILL for requestAnimationFrame
//=========================================================================

if (!window.requestAnimationFrame) { // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
  window.requestAnimationFrame = window.webkitRequestAnimationFrame ||
                                 window.mozRequestAnimationFrame    ||
                                 window.oRequestAnimationFrame      ||
                                 window.msRequestAnimationFrame     ||
                                 function(callback, element) {
                                   window.setTimeout(callback, 1000 / 60);
                                 }
}

var Game = {  // a modified version of the game loop from my previous boulderdash game - see http://codeincomplete.com/posts/2011/10/25/javascript_boulderdash/#gameloop

  run: function(options) {

    $(".tutorial-icon").click(function(e){
        e.preventDefault();
      $( "#game-tutorial" ).fadeIn( "slow", function() {
        $('#modal-close').css('display', 'block');
      });
    });

    $(".trophy-icon").click(function(e){
        e.preventDefault();
      $( "#game-high-score").fadeIn( "slow", function() {
        $('#modal-close').css('display', 'block');
        $('.game-controls').css('display', 'flex');

        var index = 1;
        window.db.collection('highScores').orderBy('score', 'desc').limit(10).get().then(function (querySnapshot) {
            $('#game-high-score .score-list').html('');
            // $('#game-high-score .score-list').html(window.fullHighScores);
            querySnapshot.forEach(function (doc) {
                var item = '<h2>' + index + '. <span>' + doc.data().name + ' -</span> ' + doc.data().score + '</h2>';
                $('#game-high-score .score-list').append(item);
                index++;
            });
        });

      });
    });

    $("#modal-close").click(function(){
      $( ".modal").fadeOut( "slow", function() {
        $('#modal-close').css('display', 'none');
        $('.game-controls').css('display', 'flex');
      });
    });

    $(".play-btn").click(function(){
      gameRunning  =  true
      $( ".game-info" ).fadeOut( "slow", function() {
        Game.playMusic();
      });
      if(gamePlayed){
        // $( ".game-info" ).fadeOut( "fast");
         resetGame();
         if(windowMute){
            Dom.storage.muted = music.muted = !music.muted;
            Dom.toggleClassName('mute', 'on', music.muted);
         }
      }else{
        Game.playMusic();
        $( ".game-info" ).fadeOut( "slow", function() {
          console.log("Go");
        });
        Game.setKeyListener(options.keys);
        if (window.DeviceOrientationEvent) {
          $( "#instuctions-text" ).html("HOLD FINGER TO SCREEN TO GO, TILT PHONE TO TURN RIGHT AND LEFT");
          window.addEventListener('deviceorientation', handleOrientation, false);
          window.addEventListener("touchstart", touchStart, false);
          window.addEventListener("touchend", touchStop, false);
          window.addEventListener("touchcancel", touchStop, false);
        }

      }
      // $.modal.close();

      // $('.modal').css('display', 'none');

      $('#game-mute').css('display', 'block');
      document.getElementById('mute-btn').classList.add("audio");
      document.getElementById('mute-img').classList.add("audio"); 

      $('#game_text').css('opacity', '1');
      $('#game_go').addClass("flash");
      setTimeout(
        function(){
          $('#game_go').removeClass("flash");
        }, 2000);

      $( "#game_text" ).fadeIn();
    });

    Game.loadImages(options.images, function(images) {
      options.ready(images); // tell caller to initialize itself because images are loaded and we're ready to rumble
      var canvas = options.canvas,    // canvas render target is provided by caller
          update = options.update,    // method to update game logic is provided by caller
          render = options.render,    // method to render the game is provided by caller
          step   = options.step,      // fixed frame step (1/fps) is specified by caller
          stats  = options.stats,     // stats instance is provided by caller
          now    = null,
          last   = Util.timestamp(),
          dt     = 0,
          gdt    = 0;

      // $('#play-game-btn').click(function(event) {

      //   console.log("play")
      // });

      function frame() {
        now = Util.timestamp();
        dt  = Math.min(1, (now - last) / 1000); // using requestAnimationFrame have to be able to handle large delta's caused when it 'hibernates' in a background or non-visible tab
        gdt = gdt + dt;
        while (gdt > step) {
          gdt = gdt - step;
          update(step);
        }
        render();
        // stats.update();
        last = now;
        requestAnimationFrame(frame, canvas);
      }
      frame(); // lets get this party started
      
    });
  },

  //---------------------------------------------------------------------------

  loadImages: function(names, callback) { // load multiple images and callback when ALL images have loaded
    var result = [];
    var count  = names.length;

    var onload = function() {
      if (--count == 0)
        callback(result);
    };

    for(var n = 0 ; n < names.length ; n++) {
      var name = names[n];
      result[n] = document.createElement('img');
      Dom.on(result[n], 'load', onload);
      result[n].src = "images/game_sprites/" + name + ".png";
    }
  },

  //---------------------------------------------------------------------------


  setKeyListener: function(keys) {
    var onkey = function(ev, keyCode, mode) {

      var n, k;
      for(n = 0 ; n < keys.length ; n++) {
        k = keys[n];

        k.mode = k.mode || 'up';
        if ((k.key == keyCode) || (k.keys && (k.keys.indexOf(keyCode) >= 0))) {
          if(gameRunning ){
            ev.preventDefault();
          }
          if (k.mode == mode) {
            k.action.call();
          }
        }
      }
    };
    // object.addEventListener("touchstart", myScript);

    Dom.on(document, 'keydown', function(ev) { onkey(ev, ev.keyCode, 'down'); } );
    Dom.on(document, 'keyup',   function(ev) { onkey(ev, ev.keyCode, 'up');   } );
  },

  //---------------------------------------------------------------------------

  stats: function(parentId, id) { // construct mr.doobs FPS counter - along with friendly good/bad/ok message box

    // var result = new Stats();
    // result.domElement.id = id || 'stats';
    // Dom.get(parentId).appendChild(result.domElement);

    // var msg = document.createElement('div');
    // msg.style.cssText = "border: 2px solid gray; padding: 5px; margin-top: 5px; text-align: left; font-size: 1.15em; text-align: right;";
    // msg.innerHTML = "Your canvas performance is ";
    // Dom.get(parentId).appendChild(msg);

    // var value = document.createElement('span');
    // value.innerHTML = "...";
    // msg.appendChild(value);

    // setInterval(function() {
    //   var fps   = result.current();
    //   var ok    = (fps > 50) ? 'good'  : (fps < 30) ? 'bad' : 'ok';
    //   var color = (fps > 50) ? 'green' : (fps < 30) ? 'red' : 'gray';
    //   value.innerHTML       = ok;
    //   value.style.color     = color;
    //   msg.style.borderColor = color;
    // }, 5000);
    // return result;
  },

  //---------------------------------------------------------------------------

  playMusic: function() {
    // console.log("play")
    var music = Dom.get('music');
    music.loop = true;
    music.volume = 1; // shhhh! annoying music!
    // music.muted = (Dom.storage.muted === "true");
    music.play();
    // Dom.toggleClassName('mute', 'on', music.muted);
    // Dom.on('mute', 'click', function() {
    //   windowMute = false
    //   Dom.storage.muted = music.muted = !music.muted;
    //   Dom.toggleClassName('mute', 'on', music.muted);
    // });
    // Dom.on('mute', 'touchstart,', function() {
    //   windowMute = false
    //   Dom.storage.muted = music.muted = !music.muted;
    //   Dom.toggleClassName('mute', 'on', music.muted);
    // });

    // window.onblur = function() {
    //   console.log("lose focus")
    //   if (music.muted == false){
    //     windowMute = true
    //     Dom.storage.muted = music.muted = !music.muted;
    //     Dom.toggleClassName('mute', 'on', music.muted);
    //   }

    //   // if (windowMute == false){
    //   //   windowMute = true;
    //   //   music.pause();
    //   //   // Dom.storage.muted = music.muted = !music.muted;
    //   //   // Dom.toggleClassName('mute', 'on', music.muted);
    //   // }
    // }
    // window.addEventListener("onfocus", function(){
    //   console.log("I'm back");
    // })
    // window.onfocus = function() {
    //   if (windowMute == false){
    //     console.log("I'm back");
    //     music.play();
    //     // windowMute = false;
    //     // Dom.storage.muted = music.muted = !music.muted;
    //     // Dom.toggleClassName('mute', 'on', music.muted);
    //   }
    // }
  }

}

//=========================================================================
// canvas rendering helpers
//=========================================================================

var Render = {

  polygon: function(ctx, x1, y1, x2, y2, x3, y3, x4, y4, color, img) {
    if (img){
      ctx.fillStyle = ctx.createPattern(img, "repeat");
    }else{
      ctx.fillStyle = color;
    }

    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.lineTo(x3, y3);
    ctx.lineTo(x4, y4);
    ctx.closePath();
    ctx.fill();
  },

  //---------------------------------------------------------------------------

  segment: function(ctx, width, lanes, x1, y1, w1, x2, y2, w2, fog, color) {

    var r1 = Render.rumbleWidth(w1, lanes),
        r2 = Render.rumbleWidth(w2, lanes),
        l1 = Render.laneMarkerWidth(w1, lanes),
        l2 = Render.laneMarkerWidth(w2, lanes),
        lanew1, lanew2, lanex1, lanex2, lane;

    if(color.grass_img){
      ctx.fillStyle = ctx.createPattern(color.grass_img, "repeat");
    }else{
      ctx.fillStyle = color.grass;
    }
    ctx.fillRect(0, y2, width, y1 - y2);

    if (color.dirt_img){
      Render.polygon(ctx, x1-w1,    y1, x1+w1, y1, x2+w2, y2, x2-w2,    y2, color.road, color.dirt_img);
      Render.polygon(ctx, x1-w1-r1, y1, x1-w1, y1, x2-w2, y2, x2-w2-r2, y2, color.rumble,  color.rumble_img);
      Render.polygon(ctx, x1+w1+r1, y1, x1+w1, y1, x2+w2, y2, x2+w2+r2, y2, color.rumble, color.rumble_img);
    }else{
      Render.polygon(ctx, x1-w1,    y1, x1+w1, y1, x2+w2, y2, x2-w2,    y2, color.road);
      Render.polygon(ctx, x1-w1-r1, y1, x1-w1, y1, x2-w2, y2, x2-w2-r2, y2, color.rumble);
      Render.polygon(ctx, x1+w1+r1, y1, x1+w1, y1, x2+w2, y2, x2+w2+r2, y2, color.rumble);
    }

    if (color.lane_img) {
      lanes = 20
      lanew1 = w1*2/lanes;
      lanew2 = w2*2/lanes;
      lanex1 = x1 - w1 + lanew1
      lanex2 = x2 - w2 + lanew2
      for(lane = 1 ; lane < lanes ; lanex1 += lanew1, lanex2 += lanew2, lane++)

        Render.polygon(ctx, lanex1 - l1/2, y1, lanex1 + l1/2, y1, lanex2 + l2/2, y2, lanex2 - l2/2, y2, color.lane, color.lane_img);
    }

    // Render.fog(ctx, 0, y1, width, y2-y1, fog);
  },

  //---------------------------------------------------------------------------

  background: function(ctx, background, width, height, layer, rotation, offset) {

    rotation = rotation || 0;
    offset   = offset   || 0;

    var imageW = layer.w/2;
    var imageH = layer.h;

    var sourceX = layer.x + Math.floor(layer.w * rotation);
    var sourceY = layer.y
    var sourceW = Math.min(imageW, layer.x+layer.w-sourceX);
    var sourceH = imageH;

    var destX = 0;
    var destY = offset;
    var destW = Math.floor(width * (sourceW/imageW));
    var destH = height;

    ctx.drawImage(background, sourceX, sourceY, sourceW, sourceH, destX, destY, destW, destH);
    if (sourceW < imageW)
      ctx.drawImage(background, layer.x, sourceY, imageW-sourceW, sourceH, destW-1, destY, width-destW, destH);
  },

  //---------------------------------------------------------------------------

  sprite: function(ctx, width, height, resolution, roadWidth, sprites, sprite, scale, destX, destY, offsetX, offsetY, clipY) {
    // console.log(sprite)
                    //  scale for projection AND relative to roadWidth (for tweakUI)
    var destW  = (sprite.w * scale * width/2) * (SPRITES.SCALE * roadWidth);
    var destH  = (sprite.h * scale * width/2) * (SPRITES.SCALE * roadWidth);

    destX = destX + (destW * (offsetX || 0));
    destY = destY + (destH * (offsetY || 0));

    var clipH = clipY ? Math.max(0, destY+destH-clipY) : 0;
    if (clipH < destH)
      ctx.drawImage(sprites, sprite.x, sprite.y, sprite.w, sprite.h - (sprite.h*clipH/destH), destX, destY, destW, destH - clipH);

  },

  //---------------------------------------------------------------------------

  player: function(ctx, width, height, resolution, roadWidth, sprites, speedPercent, scale, destX, destY, steer, updown) {

    var bounce = (1.5 * Math.random() * speedPercent * resolution) * Util.randomChoice([-1,1]);
    var sprite;
    if (steer < 0)
      sprite = (updown > 0) ? SPRITES.PLAYER_UPHILL_LEFT : SPRITES.PLAYER_LEFT;
    else if (steer > 0)
      sprite = (updown > 0) ? SPRITES.PLAYER_UPHILL_RIGHT : SPRITES.PLAYER_RIGHT;
    else
      sprite = (updown > 0) ? SPRITES.PLAYER_UPHILL_STRAIGHT : SPRITES.PLAYER_STRAIGHT;

    Render.sprite(ctx, width, height, resolution, roadWidth, sprites, sprite, scale, destX, destY + bounce, -0.5, -1);
  },

  //---------------------------------------------------------------------------

  // fog: function(ctx, x, y, width, height, fog) {
  //   if (fog < 1) {
  //     ctx.globalAlpha = (1-fog)
  //     ctx.fillStyle = COLORS.FOG;
  //     ctx.fillRect(x, y, width, height);
  //     ctx.globalAlpha = 1;
  //   }
  // },

  rumbleWidth:     function(projectedRoadWidth, lanes) { return projectedRoadWidth/Math.max(6,  2*lanes); },
  laneMarkerWidth: function(projectedRoadWidth, lanes) { return projectedRoadWidth/Math.max(32, 8*lanes); }

}

//=============================================================================
// RACING GAME CONSTANTS
//=============================================================================
// Road
var dirt1 = new Image();
dirt1.src = "images/game_sprites/dirt_1.jpg";
var dirt2 = new Image();
dirt2.src = "images/game_sprites/dirt_2.jpg";
//RoadEsges
var dirt3 = new Image();
dirt3.src = "images/game_sprites/dirt_3.gif";
var dirt4 = new Image();
dirt4.src = "images/game_sprites/dirt_4.gif";

var dust = new Image();
dust.src = "images/game_sprites/splat.png";

var grass1 = new Image();
grass1.src = "images/game_sprites/grass_1.gif";
var grass2 = new Image();
grass2.src = "images/game_sprites/grass_2.gif";

var dust1 = new Image();
dust1.src = "images/game_sprites/splat.png";

var isMobile = false;
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
  isMobile =  true;
  document.getElementById('fixed-header').classList.add("mobile")
}


var KEY = {
  LEFT:  37,
  UP:    38,
  RIGHT: 39,
  DOWN:  40,
  A:     65,
  D:     68,
  S:     83,
  W:     87
};
if( navigator.userAgent.toLowerCase().indexOf('firefox') > -1  ||  navigator.userAgent.toLowerCase().indexOf('edge') > -1 ||  navigator.userAgent.toLowerCase().indexOf('msie') > -1 || isMobile){
  var COLORS = {
    SKY:  '#72D7EE',
    TREE: '#005108',
    FOG:  '#005108',
    LIGHT:  { road: '#cab691', grass: '#607738', rumble: '#a48243', lane: 'lane'},
    DARK:   { road: '#d7c096', grass: '#586d32', rumble: '#94753d'},
    START:  { road: 'white',   grass: 'white',   rumble: 'white'},
    FINISH: { road: 'black',   grass: 'black',   rumble: 'black'},
    // LIGHT:  { road: 'light_road', grass: '#607738', rumble: 'light_rumble', lane: 'lane'  },
    // DARK:   { road: 'dark_road', grass: '#586d32', rumble: 'dark_rumble'                   },
    // START:  { road: 'white',   grass: 'white',   rumble: 'white'                     },
    // FINISH: { road: 'black',   grass: 'black',   rumble: 'black'                     }
  };

}else{
  var COLORS = {
    SKY:  '#72D7EE',
    TREE: '#005108',
    FOG:  '#005108',
    LIGHT:  { road: '#cab691', grass: '#607738', rumble: '#a48243', lane: '#d7c096', dirt_img: dirt1, rumble_img: dirt3, grass_img: grass1, lane_img: dust1},
    DARK:   { road: '#d7c096', grass: '#586d32', rumble: '#94753d', dirt_img: dirt2, rumble_img: dirt4, grass_img: grass2},
    START:  { road: 'white',   grass: 'white',   rumble: 'white'},
    FINISH: { road: 'black',   grass: 'black',   rumble: 'black'},
  }
}

var BACKGROUND = {
  HILLS: { x: 5, y:   5, w: 1280, h: 480 },
  SKY:   { x: 0, y: 495, w: 1280, h: 480 }
  // TREES: { x: 5, y: 985, w: 1280, h: 480 }
};

var SPRITES = {
  TREE1:                  { x: 600, y:  0, w: 486, h:732, name: "tree" },
  TREE2:                  { x: 700, y:  815, w: 282, h:342, name: "tree" },
  TREE3:                  { x: 1100, y:  0, w: 424, h:570, name: "tree" },
  TREE4:                  { x: 1546, y:  0, w: 400, h:574, name: "tree" },
  TREE5:                  { x: 1135, y:  688, w: 308, h:469, name: "tree" },
  TREE6:                  { x: 1444, y:  853, w: 312, h:634, name: "tree" },
  BUSH1:                  { x: 400, y:  0, w: 176, h: 168, name: "bush" },
  BUSH2:                  { x: 400, y:  1275, w: 288, h: 196, name: "bush" },
  BOULDERS:               { x: 775, y:  1275, w: 502, h: 188, name: "bush" },
  STUMP:                  { x: 400, y:  388, w: 178, h: 80, name: "bush" },
  TRASH2:                 { x: 400, y:  862, w: 147, h: 181, name: "trash" },
  TRASH1:                 { x: 400, y:  1078, w: 144, h: 170, name: "trash" },
  ROCK2:                  { x: 400, y: 694, w: 160, h: 124, name: "rock" },
  ROCK1:                  { x: 400, y: 528, w:  169, h:  110, name: "rock" },
  PLAYER_UPHILL_LEFT:     { x: 0, y: 920, w: 268, h:240 },
  PLAYER_UPHILL_STRAIGHT: { x: 0, y: 682, w: 265, h: 240 },
  PLAYER_UPHILL_RIGHT:    { x: 0, y: 1183, w:270, h:244  },
  PLAYER_LEFT:            { x: 0, y: 451, w: 267, h:224  },
  PLAYER_STRAIGHT:        { x: 0, y: 0, w: 267, h: 223},
  PLAYER_RIGHT:           { x: 0, y: 223, w:269, h:224}
};

//
SPRITES.SCALE = 0.3 * (1/SPRITES.PLAYER_STRAIGHT.w) // the reference sprite width should be 1/3rd the (half-)roadWidth

// SPRITES.BILLBOARDS = [SPRITES.BILLBOARD01, SPRITES.BILLBOARD02, SPRITES.BILLBOARD03, SPRITES.BILLBOARD04, SPRITES.BILLBOARD05, SPRITES.BILLBOARD06, SPRITES.BILLBOARD07, SPRITES.BILLBOARD08, SPRITES.BILLBOARD09];
// SPRITES.PLANTS     = [SPRITES.TREE1, SPRITES.TREE2, SPRITES.DEAD_TREE1, SPRITES.DEAD_TREE2, SPRITES.PALM_TREE, SPRITES.BUSH1, SPRITES.BUSH2, SPRITES.CACTUS, SPRITES.STUMP, SPRITES.BOULDER1, SPRITES.BOULDER2, SPRITES.BOULDER3];
// SPRITES.CARS       = [SPRITES.CAR01, SPRITES.CAR02, SPRITES.CAR03, SPRITES.CAR04, SPRITES.SEMI, SPRITES.TRUCK];

SPRITES.PLANTS     = [SPRITES.BUSH1, SPRITES.BUSH2, SPRITES.TREE1, SPRITES.TREE2, SPRITES.TREE3, SPRITES.TREE4, SPRITES.TREE5, SPRITES.TREE6, SPRITES.BOULDERS, SPRITES.STUMP];
SPRITES.CARS       = [SPRITES.TRASH1, SPRITES.ROCK1, SPRITES.ROCK2];






// GAME VARS
    var fps            = 60;                      // how many 'update' frames per second
    var step           = 1/fps;                   // how long is each frame (in seconds)
    var width          = 1024;                    // logical canvas width
    var height         = 768;                     // logical canvas height
    var centrifugal    = 0.3;                     // centrifugal force multiplier when going around curves
    var offRoadDecel   = 0.99;                    // speed multiplier when off road (e.g. you lose 2% speed each update frame)
    if( navigator.userAgent.toLowerCase().indexOf('firefox') > -1 ){
      var skySpeed       = 0.001;                   // background sky layer scroll speed when going around curve (or up hill)
      var hillSpeed      = 0.002;
    }else{
      var skySpeed       = 0.0001;                   // background sky layer scroll speed when going around curve (or up hill)
      var hillSpeed      = 0.0002;
    }
                       // background hill layer scroll speed when going around curve (or up hill)
    var treeSpeed      = 0.003;                   // background tree layer scroll speed when going around curve (or up hill)
    var skyOffset      = 0;                       // current sky scroll offset
    var hillOffset     = 0;                       // current hill scroll offset
    var treeOffset     = 0;                       // current tree scroll offset
    var segments       = [];                      // array of road segments
    var cars           = [];                      // array of cars on the road
    // var stats          = Game.stats('fps');       // mr.doobs FPS counter
    var canvas         = document.getElementById('canvas');       // our canvas...
    var ctx            = canvas.getContext('2d'); // ...and its drawing context
    var background     = null;                    // our background image (loaded below)
    var sprites        = null;                    // our spritesheet (loaded below)
    var resolution     = null;                    // scaling factor to provide resolution independence (computed)
    var roadWidth      = 2000;                    // actually half the roads width, easier math if the road spans from -roadWidth to +roadWidth
    var segmentLength  = 200;                     // length of a single segment
    var rumbleLength   = 3;                       // number of segments per red/white rumble strip
    var trackLength    = null;                    // z length of entire track (computed)
    var lanes          = 3;                       // number of lanes
    var fieldOfView    = 100;                     // angle (degrees) for field of view
    var cameraHeight   = 1000;                    // z height of camera
    var cameraDepth    = null;                    // z distance camera is from screen (computed)
    var drawDistance   = 300;                     // number of segments to draw
    var playerX        = 0;                       // player x offset from center of road (-1 to 1 to stay independent of roadWidth)
    var playerZ        = null;                    // player relative z distance from camera (computed)
    var fogDensity     = 5;                       // exponential fog density
    var position       = 0;                       // current camera Z position (add playerZ to get player's absolute Z position)
    var speed          = 0;                       // current speed
    var maxSpeed       = segmentLength/step;      // top speed (ensure we can't move more than 1 segment in a single frame to make collision detection easier)
    var accel          =  maxSpeed/5;             // acceleration rate - tuned until it 'felt' right
    var breaking       = -maxSpeed;               // deceleration rate when braking
    var decel          = -maxSpeed/5;             // 'natural' deceleration rate when neither accelerating, nor braking
    var offRoadDecel   = -maxSpeed/2;             // off road deceleration is somewhere in between
    var offRoadLimit   =  maxSpeed/4;             // limit when off road deceleration no longer applies (e.g. you can always go at least this speed even when off road)
    var totalCars      = 10;                     // total number of cars on the road
    var currentLapTime = 0;                       // current lap time
    var lastLapTime    = null;                    // last lap time

    var keyLeft        = false;
    var keyRight       = false;
    var keyFaster      = false;
    var keySlower      = false;

    var windowMute =  false


    // Scoring and Timers
    var points = 0;
    var level = 0;
    var lap = 0;
    var timeLimit = 20;
    var CheckPoints =[120000, 360000, 600000, 810000, 1150000, 1310000];
    var gameRunning = false;
    var gamePlayed =  false;
    var trashText  = "<div class='game_trash'>Trash Pickup +1000</div>"
    var trashes = []

    var hud = {
      // speed:            { value: null, dom: Dom.get('speed_value')            },
      current_lap_time: { value: null, dom: Dom.get('current_lap_time_value') },
      last_lap_time:    { value: null, dom: Dom.get('last_lap_time_value')    },
      points:    { value: null, dom: Dom.get('points_value')    }
    }

    //=========================================================================
    // UPDATE THE GAME WORLD
    //=========================================================================

    function update(dt) {

      var n, car, carW, sprite, spriteW;
      var playerSegment = findSegment(position+playerZ);
      var playerW       = SPRITES.PLAYER_STRAIGHT.w * SPRITES.SCALE;
      var speedPercent  = speed/maxSpeed;
      var dx            = dt * 2 * speedPercent; // at top speed, should be able to cross from left to right (-1 to 1) in 1 second
      var startPosition = position;

      updateCars(dt, playerSegment, playerW);

      position = Util.increase(position, dt * speed, trackLength);

      if (keyLeft)
        playerX = playerX - dx;
      else if (keyRight)
        playerX = playerX + dx;

      playerX = playerX - (dx * speedPercent * playerSegment.curve * centrifugal);

      if (keyFaster)
        speed = Util.accelerate(speed, accel, dt);
      else if (keySlower)
        speed = Util.accelerate(speed, breaking, dt);
      else
        speed = Util.accelerate(speed, decel, dt);


      if ((playerX < -1) || (playerX > 1)) {

        if (speed > offRoadLimit)
          speed = Util.accelerate(speed, offRoadDecel, dt);

        for(n = 0 ; n < playerSegment.sprites.length ; n++) {
          sprite  = playerSegment.sprites[n];
          spriteW = sprite.source.w * SPRITES.SCALE;
          if (Util.overlap(playerX, playerW, sprite.offset + spriteW/2 * (sprite.offset > 0 ? 1 : -1), spriteW)) {
            speed = maxSpeed/5;
            position = Util.increase(playerSegment.p1.world.z, -playerZ, trackLength); // stop in front of sprite (at front of segment)
            break;
          }
        }
      }

      for(n = 0 ; n < playerSegment.cars.length ; n++) {
        car  = playerSegment.cars[n];
        carW = car.sprite.w * SPRITES.SCALE;
        if (speed > car.speed) {
          if (Util.overlap(playerX, playerW, car.offset, carW, 0.8)) {
            if(car.sprite.name == "trash"){
              speed    = speed * 1.15;
              points = points + 1000
              updateHud('points', points);
              $("#game_text").append(trashText)
              setTimeout(
                function(){
                  $('.game_trash').remove();
              }, 1700);

            }else{
              speed    = speed * .15
            }
            // speed    = car.speed * (20/speed);
            car.offset = 100
            // position = Util.increase(car.z, -playerZ, trackLength);
            break;
          }
        }
      }
      if(gameRunning){
        playerX = Util.limit(playerX, -3, 3);     // dont ever let it go too far out of bounds
        speed   = Util.limit(speed, 0, maxSpeed); // or exceed maxSpeed
      }else{
        speed = 0;
      }


      skyOffset  = Util.increase(skyOffset,  skySpeed  * playerSegment.curve * (position-startPosition)/segmentLength, 1);
      hillOffset = Util.increase(hillOffset, hillSpeed * playerSegment.curve * (position-startPosition)/segmentLength, 1);
      treeOffset = Util.increase(treeOffset, treeSpeed * playerSegment.curve * (position-startPosition)/segmentLength, 1);

      if (position > playerZ  && gameRunning) {
        if (currentLapTime && (startPosition < playerZ)) {
          lap++;
          level = 0;
          // timeLimit=  timeLimit  + 20
        }
        if (currentLapTime && (CheckPoints[level] < position)) {
          if(gameRunning){
            timeLimit=  timeLimit  + 20
            level++
            points = points + 5000
            updateHud('points', points);
            $('#game_checkpoint').addClass("flash");

            setTimeout(
              function(){
                $('#game_checkpoint').removeClass("flash");
            }, 1700);



            // totalCars =  totalCars + 20

            for (var n = 0 ; n < 40 ; n++) {
              offset = Math.random() * Util.randomChoice([-0.8, 0.8]);
              z = (position +200000) + (Math.floor(Math.random() * segments.length) * segmentLength);
              sprite = Util.randomChoice(SPRITES.CARS);
              car = { offset: offset, z: z, sprite: sprite, speed: 0, };
              segment = findSegment(car.z);
              segment.cars.push(car);
              cars.push(car);
            }
          }
          // if (lastLapTime <= Util.toFloat(Dom.storage.fast_lap_time)) {
          //   Dom.storage.fast_lap_time = lastLapTime;
          //   updateHud('fast_lap_time', formatTime(lastLapTime));
          //   Dom.addClassName('fast_lap_time', 'fastest');
          //   Dom.addClassName('last_lap_time', 'fastest');
          // }
          // else {
          //   Dom.removeClassName('fast_lap_time', 'fastest');
          //   Dom.removeClassName('last_lap_time', 'fastest');
          // }
          // updateHud('last_lap_time', formatTime(lastLapTime));
          // Dom.show('last_lap_time');
        }else {
          currentLapTime += dt;
        }
      }

      // updateHud('speed', 5 * Math.round(speed/500));
      if(gameRunning){
        updateHud('current_lap_time', formatTime(timeLimit - currentLapTime));
      }

      if (gameRunning && (timeLimit - currentLapTime) < 0){

        gameRunning = false;
        updateHud('current_lap_time', "0");
        gamePlayed =  true;
        speed = 0;
        $("#final-score").html(points)
        $("#final-score-low").html(points)
        // playerZ = 0;

        let hasHighScore = false;
        window.highScores.forEach((score) => {
          if (points > score) {
            hasHighScore = true;
          }
        });
        reset();

        // console.log('hasHighScore: ', hasHighScore);

        if (hasHighScore) {
          // $('#game_text').css('opacity', '0');
          $('.game-controls').css('display', 'none')

          $("#game-race-finished").fadeIn( "slow", function() {
              $("input").keyup(function () {
                  var inputs = $(this).closest('form').find(':input');
                  inputs.eq(inputs.index(this) + 1).focus();
              });
          });

          // $('#game-race-finished').css('opacity', '1')
          // $('#game-race-finished').css('display', 'block')
          // $('#game-race-finished').modal({
          //   fadeDuration: 250,
          //   closeText: 'X'
          // });
        } else {
          $('.game-controls').css('display', 'none')
          $('#game-end-game').css('opacity', '1')
          $('#game-end-game').css('display', 'block')
          // $('#game-end-game').modal({
          //     fadeDuration: 250,
          //     closeText: 'X'
          // });
        }
        $( ".game-info" ).fadeIn( "slow", function() {

        });
      }
    }

    //-------------------------------------------------------------------------
    function touchStart(){
      keyFaster = true;
      // keySlower = false
    }
    function touchStop(){
      keyFaster = false;
    }

    function handleOrientation(event){
      // keyLeft   = true;
      // keyRight  = true;
      // keyFaster = true;
      // keySlower = true;
      // keyLeft   = false
      // keyRight  = false
      // keyFaster = false
      // keySlower = false
      // console.log(event.alpha);

      if(window.innerHeight > window.innerWidth){
        if(event.gamma > 20){
          keyRight  = true;
          keyLeft   = false;

        }else if(event.gamma < -20){
          keyRight  = false;
          keyLeft   = true;
        }else{
          keyRight  = false;
          keyLeft   = false;
        }
      }else{
        // console.log(event);
        if (event.alpha > 200){

          // if(event.alpha > 276){
          //   keyRight  = true;
          //   keyLeft   = false;
          // }else if(event.beta < 273){
          //   keyRight  = false;
          //   keyLeft   = true;
          // }else{
          //   keyRight  = false;
          //   keyLeft   = false;
          // }
        }else{
          if(event.beta > 15){
            keyRight  = true;
            keyLeft   = false;
          }else if(event.beta< -15){
            keyRight  = false;
            keyLeft   = true;
          }else{
            keyRight  = false;
            keyLeft   = false;
          }
        }
      }
    }


    function resetGame(){
      gameRunning = true;
      playerX = 0;
      points = 0
      level = 0;
      lap = 0;
      position = 0;
      speed = 0;

      timeLimit = 20;
      currentLapTime = 0;

      updateHud('points', points);

      // CheckPoints =[120000, 360000, 600000, 810000, 1150000, 1310000];
      segments = [];
      segments.length = 0
      resetRoad();

    }


    function updateCars(dt, playerSegment, playerW) {
      var n, car, oldSegment, newSegment;
      for(n = 0 ; n < cars.length ; n++) {
        car         = cars[n];
        oldSegment  = findSegment(car.z);
        car.offset  = car.offset + updateCarOffset(car, oldSegment, playerSegment, playerW);
        car.z       = Util.increase(car.z, dt * car.speed, trackLength);
        car.percent = Util.percentRemaining(car.z, segmentLength); // useful for interpolation during rendering phase
        newSegment  = findSegment(car.z);
        if (oldSegment != newSegment) {
          index = oldSegment.cars.indexOf(car);
          oldSegment.cars.splice(index, 1);
          newSegment.cars.push(car);
        }
      }
    }

    function updateCarOffset(car, carSegment, playerSegment, playerW) {

      var i, j, dir, segment, otherCar, otherCarW, lookahead = 20, carW = car.sprite.w * SPRITES.SCALE;

      // optimization, dont bother steering around other cars when 'out of sight' of the player
      if ((carSegment.index - playerSegment.index) > drawDistance)
        return 0;

      for(i = 1 ; i < lookahead ; i++) {
        segment = segments[(carSegment.index+i)%segments.length];

        if ((segment === playerSegment) && (car.speed > speed) && (Util.overlap(playerX, playerW, car.offset, carW, 1.2))) {
          if (playerX > 0.5)
            dir = -1;
          else if (playerX < -0.5)
            dir = 1;
          else
            dir = (car.offset > playerX) ? 1 : -1;
          return dir * 1/i * (car.speed-speed)/maxSpeed; // the closer the cars (smaller i) and the greated the speed ratio, the larger the offset
        }

        for(j = 0 ; j < segment.cars.length ; j++) {
          otherCar  = segment.cars[j];
          otherCarW = otherCar.sprite.w * SPRITES.SCALE;
          if ((car.speed > otherCar.speed) && Util.overlap(car.offset, carW, otherCar.offset, otherCarW, 1.2)) {
            if (otherCar.offset > 0.5)
              dir = -1;
            else if (otherCar.offset < -0.5)
              dir = 1;
            else
              dir = (car.offset > otherCar.offset) ? 1 : -1;
            return dir * 1/i * (car.speed-otherCar.speed)/maxSpeed;
          }
        }
      }

      // if no cars ahead, but I have somehow ended up off road, then steer back on
      if (car.offset < -0.9)
        return 0.1;
      else if (car.offset > 0.9)
        return -0.1;
      else
        return 0;
    }

    //-------------------------------------------------------------------------

    function updateHud(key, value) { // accessing DOM can be slow, so only do it if value has changed
      if (hud[key].value !== value) {
        hud[key].value = value;
        Dom.set(hud[key].dom, value);
      }
    }

    function formatTime(dt) {
      var minutes = Math.floor(dt/60);
      var seconds = Math.floor(dt - (minutes * 60));
      var tenths  = Math.floor(10 * (dt - Math.floor(dt)));
      if (minutes > 0)
        return minutes + "." + (seconds < 10 ? "0" : "") + seconds + "." + tenths;
      else
        return seconds + "." + tenths;
    }

    //=========================================================================
    // RENDER THE GAME WORLD
    //=========================================================================

    function render() {

      var baseSegment   = findSegment(position);
      var basePercent   = Util.percentRemaining(position, segmentLength);
      var playerSegment = findSegment(position+playerZ);
      var playerPercent = Util.percentRemaining(position+playerZ, segmentLength);
      var playerY       = Util.interpolate(playerSegment.p1.world.y, playerSegment.p2.world.y, playerPercent);
      var maxy          = height;

      var x  = 0;
      var dx = - (baseSegment.curve * basePercent);

      ctx.clearRect(0, 0, width, height);

      Render.background(ctx, background, width, height, BACKGROUND.SKY,   skyOffset,  resolution * skySpeed  * playerY);
      Render.background(ctx, background, width, height, BACKGROUND.HILLS, hillOffset, resolution * hillSpeed * playerY);
      // Render.background(ctx, background, width, height, BACKGROUND.TREES, treeOffset, resolution * treeSpeed * playerY);

      var n, i, segment, car, sprite, spriteScale, spriteX, spriteY;

      for(n = 0 ; n < drawDistance ; n++) {

        segment        = segments[(baseSegment.index + n) % segments.length];
        segment.looped = segment.index < baseSegment.index;
        // segment.fog    = Util.exponentialFog(n/drawDistance, fogDensity);
        segment.clip   = maxy;

        Util.project(segment.p1, (playerX * roadWidth) - x,      playerY + cameraHeight, position - (segment.looped ? trackLength : 0), cameraDepth, width, height, roadWidth);
        Util.project(segment.p2, (playerX * roadWidth) - x - dx, playerY + cameraHeight, position - (segment.looped ? trackLength : 0), cameraDepth, width, height, roadWidth);

        x  = x + dx;
        dx = dx + segment.curve;

        if ((segment.p1.camera.z <= cameraDepth)         || // behind us
            (segment.p2.screen.y >= segment.p1.screen.y) || // back face cull
            (segment.p2.screen.y >= maxy))                  // clip by (already rendered) hill
          continue;

        Render.segment(ctx, width, lanes,
                       segment.p1.screen.x,
                       segment.p1.screen.y,
                       segment.p1.screen.w,
                       segment.p2.screen.x,
                       segment.p2.screen.y,
                       segment.p2.screen.w,
                       segment.fog,
                       segment.color);

        maxy = segment.p1.screen.y;
      }

      for(n = (drawDistance-1) ; n > 0 ; n--) {
        segment = segments[(baseSegment.index + n) % segments.length];

        for(i = 0 ; i < segment.cars.length ; i++) {
          car         = segment.cars[i];
          sprite      = car.sprite;
          spriteScale = Util.interpolate(segment.p1.screen.scale, segment.p2.screen.scale, car.percent);
          spriteX     = Util.interpolate(segment.p1.screen.x,     segment.p2.screen.x,     car.percent) + (spriteScale * car.offset * roadWidth * width/2);
          spriteY     = Util.interpolate(segment.p1.screen.y,     segment.p2.screen.y,     car.percent);
          Render.sprite(ctx, width, height, resolution, roadWidth, sprites, car.sprite, spriteScale, spriteX, spriteY, -0.5, -1, segment.clip);
        }

        for(i = 0 ; i < segment.sprites.length ; i++) {
          sprite      = segment.sprites[i];
          spriteScale = segment.p1.screen.scale;
          spriteX     = segment.p1.screen.x + (spriteScale * sprite.offset * roadWidth * width/2);
          spriteY     = segment.p1.screen.y;
          Render.sprite(ctx, width, height, resolution, roadWidth, sprites, sprite.source, spriteScale, spriteX, spriteY, (sprite.offset < 0 ? -1 : 0), -1, segment.clip);
        }

        if (segment == playerSegment) {
          Render.player(ctx, width, height, resolution, roadWidth, sprites, speed/maxSpeed,
                        cameraDepth/playerZ,
                        width/2,
                        (height/2) - (cameraDepth/playerZ * Util.interpolate(playerSegment.p1.camera.y, playerSegment.p2.camera.y, playerPercent) * height/2),
                        speed * (keyLeft ? -1 : keyRight ? 1 : 0),
                        playerSegment.p2.world.y - playerSegment.p1.world.y);
        }
      }
    }

    function findSegment(z) {
      return segments[Math.floor(z/segmentLength) % segments.length];
    }

//=========================================================================
// BUILD ROAD GEOMETRY
//=========================================================================

    function lastY() { return (segments.length == 0) ? 0 : segments[segments.length-1].p2.world.y; }

    function addSegment(curve, y) {
      var n = segments.length;
      segments.push({
          index: n,
             p1: { world: { y: lastY(), z:  n   *segmentLength }, camera: {}, screen: {} },
             p2: { world: { y: y,       z: (n+1)*segmentLength }, camera: {}, screen: {} },
          curve: curve,
        sprites: [],
           cars: [],
          color: Math.floor(n/rumbleLength)%2 ? COLORS.DARK : COLORS.LIGHT
      });
    }

    function addSprite(n, sprite, offset) {
      segments[n].sprites.push({ source: sprite, offset: offset });
    }

    function addRoad(enter, hold, leave, curve, y) {
      var startY   = lastY();
      var endY     = startY + (Util.toInt(y, 0) * segmentLength);
      var n, total = enter + hold + leave;
      for(n = 0 ; n < enter ; n++)
        addSegment(Util.easeIn(0, curve, n/enter), Util.easeInOut(startY, endY, n/total));
      for(n = 0 ; n < hold  ; n++)
        addSegment(curve, Util.easeInOut(startY, endY, (enter+n)/total));
      for(n = 0 ; n < leave ; n++)
        addSegment(Util.easeInOut(curve, 0, n/leave), Util.easeInOut(startY, endY, (enter+hold+n)/total));
    }

    var ROAD = {
      LENGTH: { NONE: 0, SHORT:  25, MEDIUM:   50, LONG:  100 },
      HILL:   { NONE: 0, LOW:    20, MEDIUM:   40, HIGH:   60 },
      CURVE:  { NONE: 0, EASY:    2, MEDIUM:    4, HARD:    6 }
    };

    function addStraight(num) {
      num = num || ROAD.LENGTH.MEDIUM;
      addRoad(num, num, num, 0, 0);
    }

    function addHill(num, height) {
      num    = num    || ROAD.LENGTH.MEDIUM;
      height = height || ROAD.HILL.MEDIUM;
      addRoad(num, num, num, 0, height);
    }

    function addCurve(num, curve, height) {
      num    = num    || ROAD.LENGTH.MEDIUM;
      curve  = curve  || ROAD.CURVE.MEDIUM;
      height = height || ROAD.HILL.NONE;
      addRoad(num, num, num, curve, height);
    }

    function addLowRollingHills(num, height) {
      num    = num    || ROAD.LENGTH.SHORT;
      height = height || ROAD.HILL.LOW;
      addRoad(num, num, num,  0,                height/2);
      addRoad(num, num, num,  0,               -height);
      addRoad(num, num, num,  ROAD.CURVE.EASY,  height);
      addRoad(num, num, num,  0,                0);
      addRoad(num, num, num, -ROAD.CURVE.EASY,  height/2);
      addRoad(num, num, num,  0,                0);
    }

    function addSCurves() {
      addRoad(ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM,  -ROAD.CURVE.EASY,    ROAD.HILL.NONE);
      addRoad(ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM,   ROAD.CURVE.MEDIUM,  ROAD.HILL.MEDIUM);
      addRoad(ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM,   ROAD.CURVE.EASY,   -ROAD.HILL.LOW);
      addRoad(ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM,  -ROAD.CURVE.EASY,    ROAD.HILL.MEDIUM);
      addRoad(ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM,  -ROAD.CURVE.MEDIUM, -ROAD.HILL.MEDIUM);
    }

    function addBumps() {
      addRoad(10, 10, 10, 0,  5);
      addRoad(10, 10, 10, 0, -2);
      addRoad(10, 10, 10, 0, -5);
      addRoad(10, 10, 10, 0,  8);
      addRoad(10, 10, 10, 0,  5);
      addRoad(10, 10, 10, 0, -7);
      addRoad(10, 10, 10, 0,  5);
      addRoad(10, 10, 10, 0, -2);
    }

    function addDownhillToEnd(num) {
      num = num || 200;
      addRoad(num, num, num, -ROAD.CURVE.EASY, -lastY()/segmentLength);
    }


// Create Road Level
    function resetRoad() {
      segments = [];
      addStraight(ROAD.LENGTH.SHORT);
      addLowRollingHills();
      addSCurves();
      addCurve(ROAD.LENGTH.MEDIUM, ROAD.CURVE.MEDIUM, ROAD.HILL.LOW);
      addBumps();
      addLowRollingHills();
      addCurve(ROAD.LENGTH.LONG*2, ROAD.CURVE.MEDIUM, ROAD.HILL.MEDIUM);
      addStraight();
      addHill(ROAD.LENGTH.MEDIUM, ROAD.HILL.HIGH);
      addSCurves();
      addCurve(ROAD.LENGTH.LONG, -ROAD.CURVE.MEDIUM, ROAD.HILL.NONE);
      addHill(ROAD.LENGTH.LONG, ROAD.HILL.HIGH);
      addCurve(ROAD.LENGTH.LONG, ROAD.CURVE.MEDIUM, -ROAD.HILL.LOW);
      addBumps();
      addHill(ROAD.LENGTH.LONG, -ROAD.HILL.MEDIUM);
      addStraight();
      addSCurves();
      addDownhillToEnd();

      resetSprites();
      resetCars();


// Add CheckPoint
      // segments[findSegment(playerZ).index + 2].color = COLORS.START;
      // segments[findSegment(playerZ).index + 3].color = COLORS.START;

      for(var cp=0; cp < CheckPoints.length; cp++){
        // console.log(findSegment(CheckPoints[cp]))
        segments[findSegment(CheckPoints[cp]).index + 2].color = COLORS.START;
        segments[findSegment(CheckPoints[cp]).index + 3].color = COLORS.START;
      }

      // segments[findSegment(CheckPointD).index + 2].color = COLORS.START;
      // segments[findSegment(CheckPointD).index + 3].color = COLORS.START;

      // for(var n = 0 ; n < n ; n++)
      //   segments[segments.length-1-n ].color = COLORS.FINISH;

      trackLength = segments.length * segmentLength;
    }

    function resetSprites() {
      var n, i;
      for(n = 10 ; n < 200 ; n += 4 + Math.floor(n/100)) {
        addSprite(n, Util.randomChoice(SPRITES.PLANTS), -2 + Math.random()* -4);
        addSprite(n, Util.randomChoice(SPRITES.PLANTS), 1.5 + Math.random()*4);
      }

      // for(n = 250 ; n < 1000 ; n += 5) {
      //   // addSprite(n,     SPRITES.COLUMN, 0);
      //   // addSprite(n + Util.randomInt(0,5), SPRITES.TREE1, -1 - (Math.random() * 2));
      //   // addSprite(n + Util.randomInt(0,5), SPRITES.TREE2, -1 - (Math.random() * 2));
      // }

      for(n = 200 ; n < segments.length ; n += 3) {
        addSprite(n, Util.randomChoice(SPRITES.PLANTS), Util.randomChoice([1,-1]) * (2 + Math.random() * 5));
      }

      var side, sprite, offset;
      for(n = 1000 ; n < (segments.length-50) ; n += 100) {
        side      = Util.randomChoice([1, -1]);
        // addSprite(n + Util.randomInt(0, 50), Util.randomChoice(SPRITES.BILLBOARDS), -side);
        for(i = 0 ; i < 20 ; i++) {
          sprite = Util.randomChoice(SPRITES.PLANTS);
          offset = side * (1.5 + Math.random());
          addSprite(n + Util.randomInt(0, 50), sprite, offset);
        }

      }

    }

    function resetCars() {
      cars = [];
      var n, car, segment, offset, z, sprite, speed;
      for (var n = 0 ; n < totalCars ; n++) {

        offset = Math.random() * Util.randomChoice([-0.8, 0.8]);
        z = (Math.floor(Math.random() * segments.length) * segmentLength);
        sprite = Util.randomChoice(SPRITES.CARS);
        // console.log(sprite)
        // speed  = maxSpeed/4 + Math.random() * maxSpeed/(sprite == SPRITES.SEMI ? 4 : 2);
        speed = 0;
        car = { offset: offset, z: z, sprite: sprite, speed: speed, };
        segment = findSegment(car.z);
        segment.cars.push(car);
        cars.push(car);
      }
    }

    //=========================================================================
    // THE GAME LOOP
    //=========================================================================

    Game.run({
      canvas: canvas, render: render, update: update, step: step,
      images: ["background", "sprites"],
      keys: [
        { keys: [KEY.LEFT,  KEY.A], mode: 'down', action: function() { keyLeft   = true;  } },
        { keys: [KEY.RIGHT, KEY.D], mode: 'down', action: function() { keyRight  = true;  } },
        { keys: [KEY.UP,    KEY.W], mode: 'down', action: function() { keyFaster = true;  } },
        { keys: [KEY.DOWN,  KEY.S], mode: 'down', action: function() { keySlower = true;  } },
        { keys: [KEY.LEFT,  KEY.A], mode: 'up',   action: function() { keyLeft   = false; } },
        { keys: [KEY.RIGHT, KEY.D], mode: 'up',   action: function() { keyRight  = false; } },
        { keys: [KEY.UP,    KEY.W], mode: 'up',   action: function() { keyFaster = false; } },
        { keys: [KEY.DOWN,  KEY.S], mode: 'up',   action: function() { keySlower = false; } }
      ],
      ready: function(images) {
        background = images[0];
        sprites    = images[1];
        reset();
        // Dom.storage.fast_lap_time = Dom.storage.fast_lap_time || 180;
        // updateHud('fast_lap_time', formatTime(Util.toFloat(Dom.storage.fast_lap_time)));
      }
    });

    function reset(options) {
      options       = options || {};
      canvas.width  = width  = Util.toInt(options.width,          width);
      canvas.height = height = Util.toInt(options.height,         height);
      lanes                  = Util.toInt(options.lanes,          lanes);
      roadWidth              = Util.toInt(options.roadWidth,      roadWidth);
      cameraHeight           = Util.toInt(options.cameraHeight,   cameraHeight);
      drawDistance           = Util.toInt(options.drawDistance,   drawDistance);
      // fogDensity             = Util.toInt(options.fogDensity,     fogDensity);
      fieldOfView            = Util.toInt(options.fieldOfView,    fieldOfView);
      segmentLength          = Util.toInt(options.segmentLength,  segmentLength);
      rumbleLength           = Util.toInt(options.rumbleLength,   rumbleLength);
      cameraDepth            = 1 / Math.tan((fieldOfView/2) * Math.PI/180);
      playerZ                = (cameraHeight * cameraDepth);
      resolution             = height/480;
      timeLimit = 20;
      points = 0;

      if ((segments.length==0) || (options.segmentLength) || (options.rumbleLength))
        resetRoad(); // only rebuild road when necessary
    }
